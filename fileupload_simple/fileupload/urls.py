# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('fileupload_simple.fileupload.views',
    url(r'^list/$', 'list', name='list'),
)
